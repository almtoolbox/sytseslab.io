---
layout: post
title:  "The complexity of storage and caching layers"
date:   2012-10-28
---

I think it is interesting how the evolution of computing has lead to many layers of storage and caching. As a developer you have to keep a mental model of all these layers. As an example below are the approximate latencies for an instance (server) using the different services offered by [Amazon Web Services](http://en.wikipedia.org/wiki/Amazon_Web_Services#List_of_AWS_products).

1. CPU register: 0.3ns
1. L1 cache:  1ns
1. L2 cache: 3ns
1. L3 cache: 10ns
1. Main memory: 30ns
1. Local SSD: 0.1 ms
1. Local hard drive: 10ms
1. Network drive (EBS): 20ms
1. Object store (S3): 100ms
1. Tape store (Glacier): 1 hour

Luckily we don't have to think about all of the above when we are developing an application. The first five layers (from the CPU register up to main memory) can just be thought of as memory for [most of the time](http://stackoverflow.com/a/11227902/613240). And most developers don't use all services mentioned above, for example an instance on Amazon with SSD [doesn't have a local hard drive](http://aws.typepad.com/aws/2012/07/new-high-io-ec2-instance-type-hi14xlarge.html).

On the other hand there are many caching layers that application developers have to think about that happen on top of the above layers:

- Database caching (Redis and Memcache)
- Page caching (Apache / Varnish)
- Separate data centers and regions
- Content Delivery Networks
- Browser caching
- Javascript executing (asynchronous and deferred)
- Geographic latency (speed of light)
- Routing latency (internet backbone)
- Connection latency (mobile connections)

And there are even more complex techniques like [HTTP streaming](http://asciicasts.com/episodes/266-http-streaming), [precompiling assets](http://guides.rubyonrails.org/asset_pipeline.html), [asynchronous code](http://shinetech.com/thoughts/articles/139-asynchronous-code-design-with-nodejs-), [client side framworks](http://emberjs.com/) and [websockets](http://en.wikipedia.org/wiki/WebSocket). In conclusion, there a lot of [Matryoshka dolls](http://en.wikipedia.org/wiki/Matryoshka_doll) to consider when building an application.

Update: Found this awesome [time-slide overview of latencies](http://www.eecs.berkeley.edu/~rcs/research/interactive_latency.html).

# References

- [CPU and main memory latency](http://www.anandtech.com/show/2658/4)
- [SSD latency](http://en.wikipedia.org/wiki/Solid-state_drive)
- [Hard drive latency](http://pix.cs.olemiss.edu/csci423/latency)
- [Network drive latency](http://www.mysqlperformanceblog.com/2011/02/21/death-match-ebs-versus-ssd-price-performance-and-qos/)
- [Object store latency](http://readystate4.com/2012/07/09/amazon-s3-vs-amazon-cloudfront/)
- [Tape store latency](http://aws.amazon.com/glacier/faqs/)
