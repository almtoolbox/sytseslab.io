---
layout: post
title:  "Ruby on Rails routing error"
date:   2013-01-07
---

Today I solved an interesting problem in Ruby on Rails.

When I upgraded an application to a new rails version the application worked but some tests failed with "No route matches
". I couldn't figure out why. So I wanted to trace the failure down to a a specific rails commit.

I did this by using the following line in the Gemfile:
gem 'rails', :git => "git://github.com/rails/rails.git", :ref => "8af2fd889"

I varied the reference to track down the offencing commit and each time ran:
bundle update rails && rspec rspec spec/controllers/example_controller_spec.rb

Normally I use git bisect but wouldn't know how to do this in case there is another repository causing the problems. I guess you have to point bundler to a local rails repo that you run git bisect in.

In the end the problem was that format parameter was not stringified in JSON requests due to rails commit:
https://github.com/rails/rails/commit/8af2fd88917debabd3e2906a37de32a071fa8d0e

To fix it I had to change:
:format => format
to
'format' => format
