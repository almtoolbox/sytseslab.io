---
layout: post
title:  "Javascript and JSON hijacking"
date:   2013-01-10
---

Today I read a bit more about Javascript and JSON hijacking. JSON hijacking is nicely described in these articles:

[One of the first reports](http://haacked.com/archive/2009/06/25/json-hijacking.aspx)
[Security statement](http://capec.mitre.org/data/definitions/111.html)

This problem exists because browsers don't check same origin policy on script tags.

Javascript hijacking is very similar to JSON hijacking. The difference is that the server returns executable javascript instead of an executable javascript object.

Rails is vulnerable to hijacking because the CSRF protection is not enabled for GET requests.

Apparently it is hard to address this problem. There is [an old ticket for Rails](https://rails.lighthouseapp.com/projects/8994/tickets/2845-json-hijacking-some-way-to-protect-json) but right now I see no easy solution in Rails or in rich client framweorks such as Ember.

Mitigation alternatives:
- Don't send confidential data.
- Send html snippets instead of JSON.
- Send invalid data and [parse it](http://blag.7tonlnu.pl/blog/2012/09/27/json-hijacking-in-rails/).
- Use unguessable urls.
- Use a token for authentication instead of a cookie.
- Don't use GET requests.
- Use a CSRF token and fail when it is not correct.
