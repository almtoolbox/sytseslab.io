---
layout: post
title:  "Adyen for payrolling"
date:   2016-12-28
---

## Hiring people in many countries is hard

At GitLab we have [150 team members in 34 countries](https://about.gitlab.com/team/).
In the majority of countries we have contractors, in some (US/NL/UK) we have a company, in some we hire through a reseller (India/China), and in Belgium we can hire on the Dutch payroll due to the EU rules.
Setting up companies to employ people is expensive and timeconsuming.
[Remote only](http://www.remoteonly.org/) companies will become more popular and there are no great solutions.
Companies like [TriNet](http://www.trinet.com/) make hiring the the US easier.
And I've had luck in the past paying [Manpower](https://manpower.com) to employ someone in Serbia.
But there doesn't seem to be a global solution to put people on thie payroll.

## We need the Adyen of payrolling

Stipe is a good example of making a hard process (setting up credit card processing) easy.
What used to take weeks of integration now just took a few API calls.
But this problem reminds me more of the Stripe competitor Adyen.
Adyen supports local payment options outside of creditcards.
For example in the Netherlands IDeal is the most popular way to pay online.
With Adyen you don't have to set up an entity in each country, there is just one API.
I would love for someone to do this for hiring people.
If you plan to do so please email me so I can help with advise and be your first customer and investor.
