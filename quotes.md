---
layout: page
title: Quotes
permalink: /quotes/
---

I love quotations, here the ones I collected over the years.

### Quotes with attribution

"That is the greatest fallacy, the wisdom of old men. They do not grow wise. They grow careful."
-Ernest Hemingway

"Many persons have a wrong idea of what constitutes true happiness. It is not attained through self-gratification but through fidelity to a worthy purpose."
-Helen Keller

"I know of no more encouraging fact than the unquestioned ability of a man to elevate his life by conscious endeavor."
-David Thoreau

"There is no reciprocity. Men love women, women love children, children love hamsters."
-Alice Thomas Ellis

"An idealist is one who, on noticing that a rose smells better than a cabbage, concludes that it will also make better soup."
-H. L. Mencken

"People ask for criticism, but they only want praise."
-W. Somerset Maugham

"Tact is the ability to describe others as they see themselves."
-Abraham Lincoln

"A bank is a place that will lend you money if you can prove that you don't need it."
-Bob Hope

"The intelligent man finds almost everything ridiculous, the sensible man hardly anything."
-Johann Wolfgang von Goethe

"A wake up call for those who might decide to put passion a little lower on their priority scale, as it's rarely considered important or valuable, or it simply requires a lot of energy and the ability to continue to play and to accept risks."
-Stefano Mazzocchi

"You can't cheat an honest man"
-WC Fields

"It is not the critic who counts; not the man who points out how the strong man stumbles, or where the doer of deeds could have done them better. The credit belongs to the man who is actually in the arena, whose face is marred by dust and sweat and blood, who strives valiantly; who errs and comes short again and again; because there is not effort without error and shortcomings; but who does actually strive to do the deed; who knows the great enthusiasm, the great devotion, who spends himself in a worthy cause, who at the best knows in the end the triumph of high achievement and who at the worst, if he fails, at least he fails while daring greatly. So that his place shall never be with those cold and timid souls who know neither victory nor defeat."
-Theodore Roosevelt

"Trying to determine what is going on in the world by reading newspapers is like trying to tell the time by watching the second hand of a clock."
-Ben Hecht

"We think in generalities, but we live in detail."
-Alfred North Whitehead

"Fortune does not change men, it unmasks them."
-Suzanne Necker

"What we think, or what we know, or what we believe is, in the end, of little consequence. The only consequence is what we do."
-John Ruskin

"Measuring programming progress by lines of code is like measuring aircraft building progress by weight."
-Bill Gates

"We should be taught not to wait for inspiration to start a thing. Action always generates inspiration. Inspiration seldom generates action."
-Frank Tibolt

"Politics is the art of looking for trouble, finding it whether it exists or not, diagnosing it incorrectly, and applying the wrong remedy."
-Ernest Benn

"To read a newspaper is to refrain from reading something worthwhile. The first discipline of education must therefore be to refuse resolutely to feed the mind with canned chatter."
-Aleister Crowley

"Finance is the art of passing money from hand to hand until it finally disappears."
-Robert W. Sarnoff

"You can accomplish anything you want in life provided you don't mind who gets the credit."
-Harry S Truman

"A liberal is a person whose interests aren't at stake at the moment."
-Willis Player

"Judge others by their intentions and yourself by your results."
-Guy Kawasaki

"It's so simple to be wise. Just think of something stupid to say and then don't say it."
-Sam Levenson

"There is no abstract art. You must always start with something. Afterward you can remove all traces of reality."
-Pablo Picasso

"News is what someone wants to suppress, everything else is advertising."
-Reuven Frank

"You have to know how to accept rejection and reject acceptance."
-Ray Bradbury

"Fanaticism consists in redoubling your effort when you have forgotten your aim."
-George Santayana

"Freedom of the press is limited to those who own one."
-A. J. Liebling

"Laughing at our mistakes can lengthen our own life. Laughing at someone else's can shorten it."
-Cullen Hightower

"Progress isn't made by early risers. It's made by lazy men trying to find easier ways to do something."
-Robert Heinlein

"At my lemonade stand I used to give the first glass away free and charge five dollars for the second glass. The refill contained the antidote."
-Emo Phillips

"An inventor is simply a fellow who doesn't take his education too seriously."
-Charles F. Kettering

"A lie told often enough becomes the truth."
-Lenin

"A liberal is a man too broadminded to take his own side in a quarrel."
-Robert Frost

"He who praises you for what you lack wishes to take from you what you have."
-Don Juan Manuel

"The real art of conversation is not only to say the right thing at the right place but to leave unsaid the wrong thing at the tempting moment."
-Dorothy Nevill

"Never express yourself more clearly than you are able to think."
-Niels Bohr

"Americans detest all lies except lies spoken in public or printed lies."
-Edgar Watson Howe

"I love quotations because it is a joy to find thoughts one might have, beautifully expressed with much authority by someone recognized wiser than oneself."
-Marlene Dietrich

"Computer games don't affect kids, I mean if Pac Man affected us as kids, we'd all be running around in darkened rooms, munching pills and listening to repetitive music."
-Kristian Wilson

"It is good to be without vices, but it is not good to be without temptations."
-Walter Bagehot

"The world is a tragedy to those who feel, but a comedy to those who think."
-Horace Walpole

"Beware of all enterprises that require new clothes."
-Henry David Thoreau

"The purpose of life is to fight maturity."
-Dick Werthimer

"At least two-thirds of our miseries spring from human stupidity, human malice and those great motivators and justifiers of malice and stupidity: idealism, dogmatism and proselytizing zeal on behalf of religious or political ideas."
-Aldous Huxley

"Happiness is not achieved by the conscious pursuit of happiness; it is generally the by-product of other activities."
-Aldous Huxley

"When I do good, I feel good; when I do bad, I feel bad, and that is my religion."
-Abraham Lincoln

"The thing that upsets people is not what happens but what they think it means."
-Epictetus

"The people I've met who do great work rarely think that they're doing great work. They generally feel that they're stupid and lazy, that their brain only works properly one day out of ten, and that it's only a matter of time until they're found out."
-Paul Graham

"Human beings, who are almost unique in having the ability to learn from the experience of others, are also remarkable for their apparent disinclination to do so."
-Douglas Adams

"Talent hits a target no one else can hit; genius hits a target no one else can see."
-Arthur Schopenhauer

"The world is a tragedy to those who feel, but a comedy to those who think."
-Horace Walpole

"Happiness is nothing more than good health and a bad memory."
-Albert Schweitzer

"Not a shred of evidence exists in favor of the idea that life is serious."
-Brendan Gill

"The greatest obstacle to discovery is not ignorance, it is the illusion of knowledge."
-Daniel J. Boorstin

"With most men, unbelief in one thing springs from blind belief in another."
-Georg Christoph Lichtenberg

"We are continually faced with a series of great opportunities brilliantly disguised as insoluble problems."
-John W. Gardner

"The actual tragedies of life bear no relation to one's preconceived ideas. In the event, one is always bewildered by their simplicity, their grandeur of design, and by that element of the bizarre which seems inherent in them."
-Jean Cocteau

"I suppose everyone continues to be interested in the quest for the self, but what you feel when you're older, I think, is that. . . you really must make the self."
-Mary McCarthy

"Nothing strengthens authority as much as silence."
-Leonardo da Vinci

"Simplicity is the ultimate sophistication."
-Leonardo da Vinci

"I love those who can smile in trouble, who can gather strength from distress, and grow brave by reflection. It is the business of little minds to shrink, but they whose heart is firm, and whose conscience approves their conduct, will pursue their principles unto death."
-Leonardo da Vinci

"Be relaxed, then everything works."
-Jelle Zijlstra

"In public policy, it matters less who has the best arguments and more who gets heard - and by whom."
-Ralph Reed

"Religion is regarded by the common people as true, by the wise as false, and by rulers as useful."
-Seneca the Younger

"The smart way to keep people passive and obedient is to strictly limit the spectrum of acceptable opinion, but allow very lively debate within that spectrum."
-Noam Chomsky

"I may not like what you have to say, but I'll defend to the death your right to say it."
-Voltaire

"Time is a great teacher, but unfortunately it kills all its pupils."
-Hector Berlioz

"Security is mostly a superstition. It does not exist in nature."
-Helen Keller

"You live longer once you realize that any time spent being unhappy is wasted."
-Ruth E. Renkl

"To believe your own thought, to believe that what is true for you in your private heart is true for all men, that is genius."
-Ralph Waldo Emerson

"People would rather live with a problem they cannot solve rather than accept a solution they cannot understand."
-Woolsey and Swanson

"The most fortunate of persons is he who has the most means to satisfy his vagaries."
-Marquis De Sade

"The best is the enemy of good"
-Voltaire

"The truth is rarely pure and never simple."
-Oscar Wilde

"Any sufficiently advanced technology is indistinguishable from a rigged demo."
-Andy Finkel

"Simplicity is so rare and precious and so easily destroyed by good intentions that it takes a constant effort to keep the clutter from creeping in."
-Scott Christensen

"The next best thing to knowing something is knowing where to find it."
-Samuel Johnson

"I'm a sophisticated hippie"
-Tor Arne KvalÃ¸y

"Maybe the real distinguishing feature of intelligence is not awareness of one's own death, but impatience."
-Wendy M. Grossman

"Absence diminishes mediocre passions and increases great ones, as the wind blows out candles and fans fires."
-La Rochefoucauld

"I have always found that plans are useless, but planning is indispensable."
-Dwight Eisenhower

"People under the age of 25 are too young to be able to afford cynicism."
-Diogenes the Pseudo Pesky Cynic

"There is nothing so easy but that it becomes difficult when you do it reluctantly."
-Publius Terentius Afer

"Design doesn't have to be new, but it has to be good. Research doesn't have to be good, but it has to be new. The best design surpasses its predecessors by using new ideas, and the best research solves problems that are not only new, but also actually worth solving."
-Paul Graham

"The best writing is rewriting."
-E.B. White

"You can't always get what you want. But if you try sometimes, you just might find, you get what you need."
-Rolling Stones

"If A equals success, then the formula is A equals X plus Y plus Z. X is work. Y is play. Z is keep your mouth shut."
-Albert Einstein

"Victory usually goes to those green enough to underestimate the monumental hurdles they are facing."
-Richard Feynman

"If you can't say it simple and clearly, keep quiet, and keep working on it till you can."
-Karl Popper

"The best way to have a good idea is to have a lot of ideas."
-Linus Pauling

"Life shrinks or expands in proportion to one's courage."
-Anais Nin

"To be what we are, and to become what we are capable of becoming, is the only end of life."
-Robert Louis Stevenson

"It must be borne in mind that the tragedy of life does not lie in not reaching your goal. The tragedy of life lies in having no goal to reach."
-Benjamin E. Mays

"If you build it, they will come. If you propose it, they will discuss it to death"
-Abu Zeresh

"Work expands to fill the time available for its completion."
-C Northcote Parkinson

"Any sufficiently advanced technology is indistinguishable from magic."
-Arthur C. Clarke

"Argument is the worst sort of conversation."
-Jonathan Swift

"The important thing is not to stop questioning, curiosity has its own reason for existing."
-Albert Einstein

"The market can remain irrational longer than you can remain solvent."
-John Maynard Keynes

"He who wonders discovers that this in itself is wonder."
-M. C. Escher

"A fanatic is a person who can't change his mind and won't change the subject."
-Winston Churchill

"The most difficult thing in the world is to know how to do a thing and to watch someone else doing it wrong, without commenting."
-T.H. White

"A free society is one where it is safe to be unpopular."
-Adlai Stevenson

"Creation comes when you learn to say no."
-Madonna Ciccone

"Beware by whom you are called sane."
-Walter Anderson

"It is one thing to praise discipline, and another to submit to it."
-Miguel De Cervantes

"We are what we repeatedly do. Excellence then, is not an act, but a habit."
-Aristotle

"The most incomprehensible thing about the universe is that it is comprehensible."
-Albert Einstein

"It is more shameful to distrust one's friends than to be deceived by them."
-FranÃ§ois La Rochefoucauld

"Science is like sex: sometimes something useful comes out, but that is not the reason we are doing it."
-Richard Feynman

"Merely having an open mind is nothing; the object of opening the mind, as of opening the mouth, is to shut it again on something solid."
-G.K. Chesterton

"The whole problem with the world is that fools and fanatics are always so certain of themselves, but wiser people so full of doubts."
-Bertrand Russell

"We cannot reason ourselves out of our basic irrationality. All we can do is learn the art of being irrational in a reasonable way."
-Aldous Huxley

"Life is short. Forgive quickly. Kiss slowly."
-Robert Doisneau

"Have no friends not equal to yourself."
-Confucius

"Woman inspires us to great things, and prevents us from achieving them."
-Dumas

"Some people live life in the fast lane. I live life in oncoming traffic."
-Dan Kaminsky

"Do not permit a woman to ask forgiveness, for that is only the first step. The second is justification of herself by accusation of you."
-DeGourmont

"A successful tool is one that was used to do something undreamed of by its author."
-S. C. Johnson

"If you can't convince them, confuse them."
-Harry Truman

"Easy reading is hard writing."
-Ernest Hemingway

"The Christian religion has been and still is the principal enemy of moral progress in the world."
-Bertrand Russell

"Always do right. This will gratify some people and astonish the rest."
-Mark Twain

"In individuals, insanity is rare, but in groups, parties, nations, and epochs it is the rule."
-Nietzsche

"Those who will not reason, are bigots, those who cannot, are fools, and those who dare not, are slaves."
-George Gordon Noel Byron

"Religion begat prosperity, and the daughter devoured the mother."
-Cotton Mather

"When it comes to sex, men are like firemen. Always ready. Women are like fire. Not always there when you want it, but when the conditions are right, the result is magical."
-Jerry Seinfeld

"Beauty is not in the face. Beauty is a light in the heart."
-Kahlil Gibran

"The greatest thing in the world is to know how to belong to ourselves."
-Montaigne

"I was always looking outside myself for strength and confidence, but it comes from within. It was there all the time."
-Anna Freud

"Tomorrow is another day"
-Scarlett O'Hara

"The most terrifying thing is to accept oneself completely."
-C Jung

"We think in generalities, but we live in detail."
-A N Whithead

"Lust is seductive, but the passion of true love outshines it eternally."
-R Wyatt

"We can be quick-witted or very intelligent, but not both."
-Stephen Hawking

"Due to the fact there are two kinds of love, you will always have two kinds of lovers. The person you can talk with, care about and gives you a good time. And the person who makes you laugh and cry, makes you feel you're the greatest person on earth. Just make sure you pick out the right one to share your life with."
-Sandra Bleijerveld

"Psychiatry enables us to correct our faults by confessing our parents' shortcomings."
-Laurence J. Peter

"There is no surer way to ruin a good discussion than to contaminate it with the facts."
-Cecil Adams

"A true friend is one who overlooks your failures and tolerates your successes."
-Dough Larson

"Happiness in intelligent people is the rarest thing I know."
-Ernest Hemingway

"Real generosity is doing something nice for someone who will never find it out."
-Frank A. Clark

"No man can be happy without a friend, nor be sure of his friend until he is unhappy."
-Thomas Fuller

"Nothing is more difficult, and therefore more precious, than to be able to decide."
-Napoleon

"I have made mistakes, but have never made the mistake of claiming I never made one."
-James G. Bennet

"Never judge someone by who he's in love with; judge him by his friends. People fall in love with the most appalling people."
-Cynthia Heimel

"As soon as you can not keep anything from a woman, you love her."
-Paul Geraldy

"A friend is one who knows all about you and likes you anyway."
-Christi Mary Warner

"The longer the excuse, the less likely it's the truth."
-Robert Half

"When a woman is talking to you, listen to what she says with her eyes."
-Victor Hugo

"Between men and women there is no friendship possible. There is passion, enmity, worship, love, but no friendship."
-Oscar Wilde

"Never apologize for showing feeling. When you do so, you apologize for the truth."
-Benjamin Disraeli

"To love and win is the best thing. To love and lose, the next best."
-William M. Thackeray

"The life which is unexamined is not worth living."
-Plato

"The human brain is the best machine ever created for spotting patterns in noisy data. The downside of this is that if you hand it random noise it finds its own patterns."
-Paul Johnson

"First they ignore you, then they laugh at you, then they fight you, then you win."
-Mahatma Ghandi

"In order to love an other, one needs to love himself first."
-Peter Hoogenboom

"For every complex problem there is an answer that is clear, simple, and wrong."
-H L Mencken

"Computers are fast, accurate but stupid. Humans are slow, inaccurate but brilliant. Together they are powerful beyond imagination."
-Albert Einstein

"Success is getting what you want and happiness is wanting what you get."
-Warren Buffet

"If it weren't for lack of context, there would be no news."
-Scott Adams

"Goddess help me seek the truth, but spare me the company of those who've found it."
-Queen Valvolene

"Only a fool fights in a burning house."
-Kank the Klingon

"Please remember that how you say something is often more important than what you say."
-Rob Malda

"The first symptom of true love in a young man is timidity; in a young girl it is boldness."
-Victor Hugo

"In a few decades, the primary motivation for people to do anything will be fear of boredom."
-Linus Torvalds

"No one can make you feel inferior without your consent."
-Eleanor Roosevelt

"Diplomacy is the art of saying nice doggie until you can find a rock."
-Wynn Catlin

"A foolish consistency is the hobgoblin of little minds, adored by little statesmen, philosophers and divines. With consistency a great soul has simply nothing to do."
-Ralph Waldo Emerson

"Absence makes the heart grow fonder."
-Sextus Aurelius

"Opportunity, opportunity, this is your big opportunity."
-Elvis Costello

"Never attribute to malice that which can be adequately explained by stupidity."
-Robert Heinlein

"There are three kinds of lies: lies, damn lies, and statistics."
-Benjamin Disraeli

"Never mistake knowledge for wisdom. One helps you make a living; the other helps you make a life."
-Sandra Carey

"When one door closes another door opens; but we so often look so long and so regretfully upon the closed door, that we do not see the ones which open for us."
-Alexander Graham Bell

"Food isn't about Nutrition, Clothes aren't about Comfort, Bedrooms aren't about Sleep, Marriage isn't about Romance, Talk isn't about Info, Laughter isn't about Jokes, Charity isn't about Helping, Church isn't about God, Art isn't about Insight, Medicine isn't about Health, Consulting isn't about Advice, School isn't about Learning, Research isn't about Progress, Politics isn't about Policy"
-Robin Hanson

"Most people are other people. Their thoughts are someone else's opinions, their lives a mimicry, their passions a quotation."
-Oscar Wilde

"Look for three qualities: integrity, intelligence, and energy. And if they don't have the first, the other two will kill you."
-Warren Buffet

"There is a wonderful rigor in free-market economics. When you have to prove the value of your ideas by persuading other people to pay for them, it clears out an awful lot of woolly thinking."
-Tim Oâ€™Reilly

"It is generally allowed, that no man ever found the happiness of possession proportionate to that expectation which incited his desire, and invigorated his pursuit; nor has any man found the evils of life so formidable in reality, as they were described to him by his own imagination; every species of distress brings with it some peculiar supports, some unforeseen means of resisting, or powers of enduring."
-Samuel Johnson

"The test of a first-rate intelligence is the ability to hold two opposed ideas in the mind at the same time, and still retain the ability to function."
-F. Scott Fitzgerald

"Liberals think being nice excuses them for being wrong. Conservatives think being right excuses them for being mean."
-Paul Graham

"A lot of people do what they have to do. You should become an entrepreneur because you can do what you want to do."
-Chamillionaire

"Don't compare me to the Almighty, compare me to the alternative."
-Kevin White

"I never trust anyone who's more excited about success than about doing the thing they want to be successful at."
-Randall Munroe

"The fairest rules are those to which everyone would agree if they did not know how much power they would have."
-John Rawls

"Reality has a well-known liberal bias"
-Stephen Colbert

"Appreciation is a wonderful thing: It makes what is excellent in others belong to us as well."
-Voltaire

"What a strange illusion it is to suppose that beauty is goodness!"
-Leo Nikolaevich Tolstoy

"There is a great satisfaction in building good tools for other people to use."
-Freeman Dyson

"When you don't create things, you become defined by your tastes rather than ability. your tastes only narrow & exclude people. so create."
-_why

"I appreciate any conversation where I can walk away questioning myself and my ideas."
-Jack Dorsey

"Journalism is printing what someone else does not want printed: everything else is public relations."
-George Orwell

"Life can only be understood backwards; but it must be lived forwards."
-Soren Kierkegaard

"It is difficult to get a man to understand something, when his salary depends upon his not understanding it."
-Upton Sinclair

"No one is actually dead until the ripples they cause in the world die away."
- Terry Pratchett

### Quotes from unknown sources

The things you love are as stupid as the things you hate and are easily interchangeable.

Common sense is the collection of prejudices acquired by age eighteen.

Trying to be happy is like trying to build a machine for which the only specification is that it should run noiselessly.

When dealing with executives, don't complain, don't explain; they don't have the time.

The difference between stubborn and persistent lies in the external perception of your success.

If you have a hammer everything looks like a nail

With a friend you can talk all the time, with a true friend you can be silent.

Love each other for all the wrong reasons, and forgive each other for all the right ones.

Be who you are and say what you mean, for those who mind don't matter, and those who matter don't mind.

We promise according to our hopes, and perform according to our fears.

One of the most baffling things about getting older is how shocking we find it that anyone could be too young to remember things we remember.

The basis of all love is respect.

Tact is the art of making a point without making an enemy.

You don't become a failure until you're satisfied with being one.

Subtlety is the art of saying what you think and getting out of the way before it is understood.

You've got to dance like nobody's watching, and love like it's never going to hurt.

I will not drag you along, I will not leave you alone; I will stand by you and have my hand there for you to hold when you need to.

A real friend is someone who treat you kindly behind your back.

And the robot says: "In the beginning was man. Man created all things. Man, with his infinite skill, created machines in his own image.

A real friend is someone who knows all about you and still respects you.

You can make more friends in two months by becoming interested in other people than you can in two years by trying to get other people interested in you.

When a friend is in trouble, don't annoy him by asking if there is anything you can do. Think up something appropriate and do it.

Once you get people laughing, they're listening and you can tell them almost anything.

The most important thing in communication is to hear what isn't being said.

If you teach someone 'what' to think, you make them a slave to your knowledge. If you teach someone 'how' to think, you make all knowledge their slave.

Learning is understanding something you've understood all your life, but in a new way.

Faster, Better, Cheaper; pick any two.

He who knows little quickly tells it.

If you want to be respected, you must respect yourself.

The best index to a person's character is the way he treats people who can't do him any good or can't fight back.

If you can't smile when things go wrong, you've someone in mind to blame.

Absence is to love what wind is to fire. It extinguishes the small, it enkindles the great.

He who asks is a fool for five minutes, but he who does not ask remains a fool forever.

Advice is what we ask for when we already know the answer but wish we didn't.

You will never find time for anything. If you want time you must make it.

Never argue over unimportant details. Even if you win, you'll have gained no advantage.

Character is what you know you are, not what other think you have.

Fear can hold you prisoner, hope can set you free.

Nothing is interesting if you are not interested.

Complain to one who can help you.

We have strange and wonderful relationship. You're strange and I'm wonderful.

What's right isn't always popular, what's popular isn't always right.

Worry takes as much time as work and pays less.

Failures are divided into two categories: Those who thought and never did, and those who did and never thought.

If you don't invest very much, then defeat doesn't hurt very much and winning is not very exciting.

The whole point of getting things done is knowing what to leave undone.

To avoid criticism, do nothing, say nothing, be nothing.

He that never changes his opinions, never corrects his mistakes, will never be wiser tomorrow than he is today.

Business is like riding a bicycle. Either you keep moving or you fall down.

If you can't find the time to do it right the first time, when are you going to find time to do it over?

Discoveries are made by not following instructions.

The climb upwards will be easier if you take others with you.

Our greatest glory is not in never failing, but in rising up every time we fail.

We are born with our eyes closed and our mouths open, and we spend our whole lives trying to reverse that mistake of nature.

Don't dream a dream, live a dream and let reality sleep.

I would rather live in a world where my life is surrounded by mystery than live in a world so small that my mind could comprehend it.

I always wanted to be somebody, but I see now I should have been more specific.

If you don't run your own life, somebody else will.

The surest sign that intelligent life exists elsewhere in the universe is that it has never tried to contact us.

The first sign of a nervous breakdown is when you start thinking your work is terribly important.

If you and your partner always agree, one of you is unnecessary.

Good judgment comes from experience, and experience... well, that comes from poor judgment.

Be smarter than other people, just don't tell them so.

There are things so serious that you can only joke about them.

Children are the most expensive form of entertainment.

I hope life isn't a big joke...Because I don't get it.

Talk is cheap because supply exceeds demand.

A conclusion is simply the place where you got tired of thinking.

A synonym is a word you use when you can't spell the other.

Democracy is three wolves and one sheep voting on what to have for supper.

Always remember you're unique, just like everyone else.

The only thing worse than infinite recursion is infinite recursion.

In theory, theory and practice are the same, but in practice they're different.

A year spent in artificial intelligence is enough to make one believe in God.

People who work hard make many mistakes, people who work little make few mistakes, some people don't make any mistakes.

Never argue with fools, they don't know when you win.

You can love someone and not like the way they act.

It's easier to glorify the past then to work on the future.

Sex is sweet, death is bitter, love is both.

Smart people learn from their mistakes, wise people learn from others' mistakes.

A bore is a man who, when you ask him how he is, he tells you.

Minds are like parachutes... they work best when open.

Great minds discuss ideas, average minds discuss events, small minds discuss people.

### Quotes from Sytse Sijbrandij

"If it wasn't for the deadline I would probably not take the trouble to live my life."

"A man without a plan is just a man."

"Life is a non-zero-sum game."

"In a knowledge economy being different makes someone more valuable."

"If you already know something, why bother to write it down?"

"It takes a genius to understand simplicity."

"When you expect nothing more then love itself, it can grow without fear of ending."

"Poems are a mind exposed."

"In times of great fear your true nature shows."

"People who laugh because I'm different make me smile because they're all the same."

"Friends tell you what you already know but don't want to hear."

"People want hero's, so why not be one."

"They don't want the truth, they want something to believe in."

"Always consider failing an option, that way you can choose otherwise."

"Being an entrepreneur means doing risky things the most risk-free way."

"Thinking you a great person is considered positive behavior as long as you respect others."

"Kids can be most cruel since they can act cruel without intending to do so."

"When a woman wants to talk to you she'll find a subject to speak about."

"Laugh and the world laughs with you. Cry and they'll ignore you."

"When love is no longer a compromise, you've met someone for the rest of your life."

"You don't get a life, you make one."

"A true lover lets you feel the beautiful person inside you."

"With love, life and sex, don't plan, don't have a goal, just be glad for the moment."

"In this live you don't have to fulfill any other expectations then your own."

"Never let someone else decide what you're worthy of."

"Intelligent people always ask themselves why where others would just walk by."

"Money is a shallow destination, it is love that gives depth to the ship of life."

"Laugh at life and it will smile back."
